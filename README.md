Credit: Yaml file template credit goes to: https://github.com/jsidrach/resume-generator. 

# What
This application uses [PyLatex](https://jeltef.github.io/PyLaTeX/current/), Python3, and Python Yaml to create a Resume. 

## Dependencies: 
* Python3 - `sudo s/apt-get/yum/dnf/<whatever your distro requires install python`
* PyLatex - `sudo pip3 install pylatex pyyaml`
* Texlive - `sudo s/apt-get/yum/dnf/<whatever your distro requires> install texlive`

Note: This project assumes a linux environment. There's no reason this code should not work on windows with the properly installed dependencies, but it has not been tested on windows. 

Note: Extra LaTeX packages may need to be installed. 

## How To
Edit the `resume.yml` file to reflect your resume, and then run `python3 resume.py`

Output is a PDF, and a TEX file. Tex files can be edited in texmaker, lyx, or any other latex editor! Have fun!

Example PDF (which has been converted to a jpg for this README): 

![header.pdf](/resume.jpg)

# Credit: 
Credit: Yaml file template credit goes to: https://github.com/jsidrach/resume-generator. 
