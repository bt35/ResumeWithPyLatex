##### 
# Latex by itself is complicated to me. Python isn't. 
# This is my attempt at resume writing. 
# Author: bt35
# https://gitlab.com/bt35/ResumeWithPyLatex
#####

from pylatex import Document, PageStyle, Head, MiniPage, Foot, LargeText, \
    MediumText, LineBreak, simple_page_number, Section, HFill, NewLine, Package, \
    TextBlock, Itemize, Enumerate
from pylatex.utils import bold, NoEscape, italic
import yaml

geometry_options = {"margin": "0.7in", "bottom" : "0.3in"}

#horizontal rule
rule = r'\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.9pt}}'
sec_rule = r'\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}'

brk = NoEscape(r'\\')

with open('resume.yml') as stream:
    info = yaml.load(stream)


def header(doc):
    doc.header = PageStyle("header")
    with doc.header.create(Head("C")):
        doc.header.append(LargeText(bold(info['name'])))
        doc.header.append(LineBreak())
        doc.header.append(info['contact']['address'])
        doc.header.append(LineBreak())
        doc.header.append(info['contact']['phone'])
        doc.header.append(" | ")
        doc.header.append(info['contact']['email'])
        doc.header.append(" | ")
        doc.header.append(info['contact']['linkedin']['url'])
        doc.header.append(LineBreak())
        doc.header.append(NoEscape(rule))

        doc.preamble.append(doc.header)
        doc.change_document_style("header")

# Add Heading
    with doc.create(MiniPage(align='c')):
        doc.append(MediumText(bold("")))


def sections(doc):
        doc.append(LineBreak())
        doc.append(bold("Objective: "))
        doc.append(info.get('summary'))
        for entry in info['sections']:
            a = r'\underline{'
            b = a + entry['name']
            section_name = b + '}'
            doc.append(NoEscape(r'\titlespacing*{\section}{0pt}{*1}{*1}'))
            with doc.create(Section(NoEscape(section_name), numbering=False)):
                for item in entry['entries']:
                    doc.append(NoEscape(r'\begin{flushleft}'))
                    if item.get("what"):
                        doc.append(bold(item.get("what")))
                        doc.append(brk)
                    if item.get("where"):
                        doc.append(italic(item.get("where")))
                        doc.append(brk)
                    if item.get("when"):
                        doc.append(item.get("when"))
                        doc.append(brk)
                    if item.get("description"):
                        doc.append(italic(item.get("description")))
                        doc.append(brk)
                    if item.get("details"):
                        for i in item.get("details"):
                            doc.append(NoEscape(r'\textbullet'))
                            doc.append(" ")
                            doc.append(i)
                            doc.append(brk)
                    if item.get("url"):
                        doc.append(NoEscape(r'\Mundus~\url{' + item.get("url") + "}"))
                        doc.append(brk)
                        print(item.get("url"))
                    
                    doc.append(NoEscape(r'\end{flushleft}'))
            
if __name__ == '__main__':

    doc = Document(geometry_options=geometry_options)
    doc.packages.append(Package('titlesec', 'compact'))
    doc.packages.append(Package('bookman'))
    doc.packages.append(Package('marvosym'))
    doc.packages.append(Package('hyperref'))
    header(doc)
    sections(doc)
    doc.generate_pdf("header", clean_tex=False)

